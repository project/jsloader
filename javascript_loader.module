<?php
/**
 * @file
 * An API to load JavaScript files asyncronously.
 */

/**
 * Implementation of hook_js_alter().
 *
 * Adds `jsloader` type support. For items that are `type` `jsloader`:
 *  * the name aka `data` field is recommended to be `component.jsloader`,
 *    where component is the JavaScript component about to be added.
 *  * the `jsload` array is reqired and can have the folling fields:
 *    * paths - an array of paths to the script files to be loaded.
 *    * prefix - the JavaScript code to be inserted before the loading.
 *    * allow_duplicates - allow to add load same file multiple times.
 *    * callback - the JavaScript code to be called
 *                 after the file(s) stated in `paths` are loaded.
 *
 * Example:
 *   drupal_add_js(
 *     'example.jsloader',
 *     array(
 *       'type' => 'jsloader',
 *       'scope' => 'header',
 *       'jsloader' => array(
 *         'prefix' => 'var Drupal.example=function () { alert("example"); };',
 *         'paths' => array('path/to/example.js'),
 *         'callback' => 'Drupal.example();',
 *         'library' => 'labjs'
 *       ),
 *       'weight' => 10,
 *     )
 *   );
 * At the moment loading the JSLoad library is needed for the code to work.
 * To load the library:
 *   drupal_add_library('javascript_loader', 'JSLoad');
 *
 * @todo Make the JSLoad library load automatically.
 * @param $scripts
 *   Array of scripts served by drupal_get_js() / drupal_add_js().
 */
function javascript_loader_js_alter(&$scripts) {
  static $loaded_libs = array();

  // Defaults for the to be added JSLoad inline code.
  $defaults = drupal_js_defaults('');
  $defaults['type'] = 'inline';
  $default_library = 'labjs';
  $jsload_list = array();

  // Go through the scripts and check for `jsload` type items.
  foreach ($scripts as $key => $script) {
    if ($script['type'] == 'jsloader') {
      $jsload_key = 'JSLoad.' . $script['scope'];

      // Prepare default settings for the inline script.
      if (empty($scripts[$jsload_key])) {
        $scripts[$jsload_key] = array_merge($defaults);
        $scripts[$jsload_key]['scope'] = $script['scope'];
      }
      $library = empty($scripts['library']) ? $default_library : $scripts['library'];

      if (!in_array($library, $loaded_libs)) {
        $loaded_libs[] = $library;
        $lib_data = drupal_get_library('javascript_loader', $library);
        if ($lib_data && !empty($lib_data['js'])) {
          foreach ($lib_data['js'] as $data => $options) {
            $options += drupal_js_defaults($data);
            $scripts[$data] = $options;
          }
        }
      }

      // Store data for future processing.
      $jsload_list[$jsload_key][$key] = array(
        'weight' => $script['weight'],
        'data'   => implode("\n", module_invoke_all('jsloader_prepare', $library, $script['jsloader'])),
        'library' => $library,
      );
    }
  }

  if (!empty($jsload_list)) {
    // Go through the inline codes for each scope
    foreach ($jsload_list as $key => $script) {
      // Sort the codes, and add them to the prepared inline script.
      uasort($script, 'drupal_sort_weight');
      $data = array();
      foreach($script as $val) {
        $data[] = $val['data'];
      }

      $scripts[$key]['data'] = implode("\n", $data);
    }
  }
}

/**
 * Implementation of hook_jsloader_prepare().
 */
function javascript_loader_jsloader_prepare($library, $settings) {
  $data = '';
   // Prepare inline code to be added for this specific item.
  if (!empty($settings['prefix'])) {
    $data = $settings['prefix'] .";\n";
  }
  switch ($library) {
    case 'labjs':
      $data .= '$LAB.script('. json_encode($settings['paths']) . ')';
      if (!empty($settings['callback'])) {
        $data .= '.wait(function () {' . $settings['callback'] . ';})';
      }
      return $data . ';';

    case 'loadjs':
      $data .= 'loadJS.load('. implode(', ', array_map('json_encode', $settings['paths'])) . ')';
      if (!empty($settings['callback'])) {
        $data .= ".thenDefer(10).thenRun(function () {\n{$settings['callback']};\n})";
      }
      return $data . ';';

    case 'headjs':
      $data .= 'head.js('. implode(', ', array_map('json_encode', $settings['paths']));
      if (!empty($settings['callback'])) {
        $data .= ", function () {\n{$settings['callback']};\n}";
      }
      return $data . ');';

    case 'scriptjs':
      $data .= '$script(' . json_encode($settings['paths']) . ', ';
      if (!empty($settings['callback'])) {
        $data .= ", function () {\n{$settings['callback']};\n}";
      }
      return $data . ');';

    case 'curljs':
      $paths = array();
      foreach ($settings['paths'] as $path) {
        $paths[] = 'js!' . $path;
      }
      $data .= 'require(' . json_encode($paths) . ')';
      if (!empty($settings['callback'])) {
        $data .= ".then(function () {\n{$settings['callback']};\n}, function () {})";
      }
      return $data . ';';
  }
}

/**
 * Implementation of hook_library().
 */
function javascript_loader_library() {
  global $theme;

  $path = drupal_get_path('module', 'javascript_loader');

  drupal_theme_initialize();
  $themes = list_themes();
  $scopes = !empty($themes[$theme]->info['scopes']) ? $themes[$theme]->info['scopes'] : array('header', 'footer');
  $scope = reset($scopes);
  $weight = -9999;

  $libraries['labjs'] = array(
    'title'   => 'LABjs',
    'version' => '1.2.0',
    'website' => 'http://labjs.com/',
    'js'      => array(
      $path . '/libraries/LAB.js' => array(
        'scope' => $scope,
        'weight' => $weight,
        'preprocess' => TRUE,
        'every_page' => TRUE,
      ),
    ),
    'css'     => array(),
  );
  $libraries['loadjs'] = array(
    'title'   => 'load.js',
    'version' => '1.0',
    'website' => 'https://github.com/Poetro/load.js',
    'js'      => array(
      $path . '/libraries/load-min.js' => array(
        'scope' => $scope,
        'weight' => $weight,
        'preprocess' => TRUE,
        'every_page' => TRUE,
      ),
    ),
    'css'     => array(),
  );
  $libraries['headjs'] = array(
    'title'   => 'Head JS',
    'version' => '0.9',
    'website' => 'http://headjs.com/',
    'js'      => array(
      $path . '/libraries/head.min.js' => array(
        'scope' => $scope,
        'weight' => $weight,
        'preprocess' => TRUE,
        'every_page' => TRUE,
      ),
    ),
    'css'     => array(),
  );
  $libraries['scriptjs'] = array(
    'title'   => '$script.js',
    'version' => '1.0.1',
    'website' => 'http://dustindiaz.com/scriptjs',
    'js'      => array(
      $path . '/libraries/script.min.js' => array(
        'scope' => $scope,
        'weight' => $weight,
        'preprocess' => TRUE,
        'every_page' => TRUE,
      ),
    ),
    'css'     => array(),
  );
  $libraries['curljs'] = array(
    'title'   => 'curl.js',
    'version' => '0.2',
    'website' => 'https://github.com/unscriptable/curl',
    'js'      => array(
      $path . '/libraries/curl.js' => array(
        'scope' => $scope,
        'weight' => $weight,
        'preprocess' => TRUE,
        'every_page' => TRUE,
      ),
    ),
    'css'     => array(),
  );
  return $libraries;
}
