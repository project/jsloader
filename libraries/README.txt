Download one of the following libraries to this folder:

load.js
=======================================
URL: https://github.com/Poetro/load.js
filename: load-min.js

Head JS
=======================================
URL: http://headjs.com/
filename: head.min.js

LABjs
=======================================
URL: http://labjs.com/
filename: LAB.js

$script.js
=======================================
URL: http://dustindiaz.com/scriptjs
filename: script.min.js

curl.js
=======================================
URL: https://github.com/unscriptable/curl
filename: curl.js